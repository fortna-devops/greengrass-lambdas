#
# Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#

# greengrassHelloWorld.py
# Demonstrates a simple publish to a topic using Greengrass core sdk
# This lambda function will retrieve underlying platform information and send
# a hello world message along with the platform information to the topic 'hello/world'
# The function will sleep for five seconds, then repeat.  Since the function is
# long-lived it will run forever when deployed to a Greengrass core.  The handler
# will NOT be invoked in our example since the we are executing an infinite loop.

import greengrasssdk
import platform
from threading import Timer
import json
import arrow
import hashlib
import os
import sys, logging, time
import cpppo
from cpppo.server.enip import address, client
import plc_enip
import csv

# Creating a greengrass core sdk client
client = greengrasssdk.client('iot-data')

# Retrieving platform information to send from Greengrass Core
my_platform = platform.platform()

# When deployed to a Greengrass core, this code will be executed immediately
# as a long-lived lambda function.  The code will enter the infinite while loop
# below.
# If you execute a 'test' on the Lambda Console, this test will fail by hitting the
# execution timeout of three seconds.  This is expected as this function never returns
# a result.


def lambda_handler():


    gateway = os.environ['gateway']
    port = os.environ['port']
    plc_tags = json.loads(os.environ['plc_tags'])
    topic = os.environ['topic']
    site = os.environ['site']
    read_interval = int(os.environ['read_interval'])
    
    try:
        plc = plc_enip.PLC_ENIP(host='192.168.1.5', route_path=[{"link": 2, "port": 1}])
        with plc.get_connection() as connection:
            for tag in plc_tags:
                raw_data = plc.read_plc_string(connection, tag)
                
                csv_reader = csv.reader([raw_data])
                data = list(csv_reader)[0]
                
                payload = {}
                
                payload['conveyor']=data[0]
                payload['plc_time']=data[1]
                payload['plc_status']=data[2]
                payload['belt_speed']=data[3]
                payload['motor_current']=data[4]
                payload['motor_freq']=data[5]
                payload['motor_voltage']=data[6]
                payload['vfd_fault_code']=data[7]
                payload['induct_rate']=data[8]
                payload['reject_rate']=data[9]
                payload['conveyor_status']=data[10]
                
                md5 = hashlib.md5()
                
                payload['gateway'] = gateway
                payload['port'] = port
                payload['site'] = site
                payload['sensor'] = 'PLC-'+payload['conveyor']
                
                md5.update(payload['gateway']+'_'+payload['port']+'_'+payload['sensor'])
                payload['hash_key'] = md5.hexdigest()
                
                payload['read_time'] = arrow.now().isoformat()
                client.publish(topic=topic, payload=json.dumps(payload))
    except Exception as e:
        payload = {}
        payload['sensor_error'] = str(e)
        
        md5 = hashlib.md5()
        
        payload['gateway'] = gateway
        payload['port'] = port
        payload['site'] = site
        payload['conveyor'] = tag.replace('_UPLOAD','').replace('_','-')
        payload['sensor'] = 'PLC-'+payload['conveyor']
        
        md5.update(payload['gateway']+'_'+payload['port']+'_'+payload['sensor'])
        payload['hash_key'] = md5.hexdigest()
        
        payload['read_time'] = arrow.now().isoformat()
        client.publish(topic=topic, payload=json.dumps(payload))
    # Asynchronously schedule this function to be run again in 5 seconds
    Timer(read_interval, lambda_handler).start()


# Start executing the function above
lambda_handler()


# This is a dummy handler and will not be invoked
# Instead the code above will be executed in an infinite loop for our example
def function_handler(event, context):
    return