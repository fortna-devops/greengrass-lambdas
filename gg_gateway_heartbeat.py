#
# Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#

# greengrassHelloWorld.py
# Demonstrates a simple publish to a topic using Greengrass core sdk
# This lambda function will retrieve underlying platform information and send
# a hello world message along with the platform information to the topic 'hello/world'
# The function will sleep for five seconds, then repeat.  Since the function is
# long-lived it will run forever when deployed to a Greengrass core.  The handler
# will NOT be invoked in our example since the we are executing an infinite loop.

import greengrasssdk
import platform
from threading import Timer
import time
import json
import arrow
import os
import hashlib


# Creating a greengrass core sdk client
client = greengrasssdk.client('iot-data')

# Retrieving platform information to send from Greengrass Core
my_platform = platform.platform()

# When deployed to a Greengrass core, this code will be executed immediately
# as a long-lived lambda function.  The code will enter the infinite while loop
# below.
# If you execute a 'test' on the Lambda Console, this test will fail by hitting the
# execution timeout of three seconds.  This is expected as this function never returns
# a result.

# Override Defaults
# Set port baud rate

def lambda_handler():
    
    site =  os.environ['site']
    gateway = os.environ['gateway']
    interval = int(os.environ['interval'])
    topic = os.environ['topic']
    folder = os.environ['folder']
    
    md5 = hashlib.md5()
    md5.update(site+'_'+gateway)
    hash_key = md5.hexdigest()
    
    try:
        with open(folder+"signal_quality.txt", "r") as signal_quality:
            payload = json.loads(signal_quality.read())
            signal_quality.close()
            payload['hash_key']=hash_key
            payload['site']=site
            payload['gateway']=gateway
            payload['heartbeat_time'] = arrow.now().isoformat()
            client.publish(topic=topic, payload=json.dumps(payload))

except Exception as e:
    payload = {}
        payload['hash_key']=hash_key
        payload['site']=site
        payload['gateway']=gateway
        payload['error']=str(e)
        payload['heartbeat_time'] = arrow.now().isoformat()
        client.publish(topic=topic, payload=json.dumps(payload))
    # Asynchronously schedule this function to be run again in 5 seconds
    Timer(interval, lambda_handler).start()


# Start executing the function above
lambda_handler()


# This is a dummy handler and will not be invoked
# Instead the code above will be executed in an infinite loop for our example
def function_handler(event, context):
    return
