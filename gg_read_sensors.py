#
# Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#

# greengrassHelloWorld.py
# Demonstrates a simple publish to a topic using Greengrass core sdk
# This lambda function will retrieve underlying platform information and send
# a hello world message along with the platform information to the topic 'hello/world'
# The function will sleep for five seconds, then repeat.  Since the function is
# long-lived it will run forever when deployed to a Greengrass core.  The handler
# will NOT be invoked in our example since the we are executing an infinite loop.

import greengrasssdk
import platform
from threading import Timer
import time
import minimalmodbus
import json
import arrow
import banner_qm42vt2
import hashlib
import os

# Creating a greengrass core sdk client
client = greengrasssdk.client('iot-data')

# Retrieving platform information to send from Greengrass Core
my_platform = platform.platform()

# When deployed to a Greengrass core, this code will be executed immediately
# as a long-lived lambda function.  The code will enter the infinite while loop
# below.
# If you execute a 'test' on the Lambda Console, this test will fail by hitting the
# execution timeout of three seconds.  This is expected as this function never returns
# a result.

# Override Defaults
# Set port baud rate
minimalmodbus.BAUDRATE = 19200
minimalmodbus.TIMEOUT = 1

def read_sensors():

    sensors = []
    
    port1 = {'key':'1_rs232', 'device':'/dev/ttyS6'}
    port2 = {'key':'2_rs422_485', 'device':'/dev/ttyS4'}
    port3 = {'key':'3_rs485', 'device':'/dev/ttyS5'}
    port4 = {'key':'4_rs485', 'device':'/dev/ttyS2'}

    ports = {}
    ports[port1['key']] = port1
    ports[port2['key']] = port2
    ports[port3['key']] = port3
    ports[port4['key']] = port4

    gateway = os.environ['gateway']
    port = os.environ['port']
    sensor_ids = json.loads(os.environ['sensor_ids'])
    topic = os.environ['topic']
    site = os.environ['site']
    read_interval = int(os.environ['read_interval'])
    
    for i in sensor_ids:
        try:
            sensor = banner_qm42vt2.BannerQM42VT2(ports[port]['device'], i)
            payload = sensor.get_sensor_values()
            md5 = hashlib.md5()
            
            payload['gateway'] = gateway
            payload['port'] = ports[port]['key']
            payload['site'] = site
            payload['sensor'] = str(i)
            
            md5.update(payload['gateway']+'_'+payload['port']+'_'+payload['sensor'])
            payload['hash_key'] = md5.hexdigest()
            
            payload['read_time'] = arrow.now().isoformat()
            client.publish(topic=topic, payload=json.dumps(payload))
        except Exception as e:
            payload = {}
            payload['sensor_error'] = str(e)
            md5 = hashlib.md5()
            
            payload['gateway'] = gateway
            payload['port'] = ports[port]['key']
            payload['site'] = site
            payload['sensor'] = str(i)
            
            
            md5.update(payload['gateway']+'_'+payload['port']+'_'+payload['sensor'])
            payload['hash_key'] = md5.hexdigest()
            
            payload['read_time'] = arrow.now().isoformat()
            client.publish(topic=topic, payload=json.dumps(payload))
    # Asynchronously schedule this function to be run again in 5 seconds
    Timer(read_interval, read_sensors).start()


# Start executing the function above
read_sensors()


# This is a dummy handler and will not be invoked
# Instead the code above will be executed in an infinite loop for our example
def function_handler(event, context):
    return