#!/usr/bin/env python
import minimalmodbus
import time
import banner_qm42vt2
import sys


minimalmodbus.BAUDRATE = 19200
#19200
#minimalmodbus.TIMEOUT = 0.1
# port name, slave address (in decimal)

sensors = []
start=1
count=247

port = '/dev/ttyS6'

for i in range(start,start+count):
    sensor = banner_qm42vt2.BannerQM42VT2(port, i)
#    sensor.debug=True
#    sensor.precalculate_read_size=False
#    sensor.serial.stopbits=2
    sensors.append(sensor)
#print(sensor)
#for i in range(0,len(sensors)):
#    print(sensors[i])


while True:
    # Register number, number of decimals, function code
    start_time=time.time()
    for i in range(0,len(sensors)):
        try:
            values = sensors[i].get_sensor_values()
            
            print("Slave ID " + str(sensors[i].address)+" "+str(values))
            print("Sensor ID is "+str(sensors[i].address))
            exit()
        except Exception as e:
            print(str(sensors[i].address))
            #print(str(e))
            #print("unable to read slave id " + str(sensors[i].address))
    print("--------------------Time = "+str(round(time.time()-start_time,2))+" seconds----------------------")
    print("")
    time.sleep(5)
