#!/usr/bin/env python

"""

.. moduleauthor:: Dipen Pradhan <dipen_pradhan@mckinsey.com>

Driver for the Banner Banner QM42VT2 vibration and temperature sensor, for communication via the Modbus RTU protocol.

"""

import minimalmodbus

__author__ = "Dipen Pradhan"
__email__ = "dipen_pradhan@mckinsey.com"
__license__ = "Proprietary Software"


class SeaLevelSeaIO450M(minimalmodbus.Instrument):
    """Instrument class for SeaLevel SeaI/O 450M Relay.

    Communicates via Modbus RTU protocol (via RS485), using the *MinimalModbus* Python module.

    
    Args:
        * portname (str): port name
        * slaveaddress (int): slave address in the range 1 to 247

    Implemented with these function codes (in decimal):

    ==================  ====================
    Description         Modbus function code
    ==================  ====================
    Read bit              1
    Write bit             5
    ==================  ====================

    """

    def __init__(self, portname, slaveaddress):
        minimalmodbus.Instrument.__init__(self, portname, slaveaddress)

    
    def change_bit(self, relay_id, value):
        
        relay_id = int(relay_id)

        if relay_id <0 or relay_id>15:
            raise ValueError('Relay port number incorrect. Can only be between 0-15')
        elif value<0 or value>1:
            raise ValueError('Incorrect value set for bit. Can onl be 0 or 1')
        else:
            self.write_bit(relay_id,value,5)
    
    def is_open_NC(self, relay_id):
        """Return if connection is OPEN (device is OFF) in NC mode."""        
        return True if self.read_bit(relay_id,1) == 1 else False

    def is_close_NO(self, relay_id):
        """Return if connection is CLOSED (device is ON) in NO mode."""        
        return True if self.read_bit(relay_id,1) == 1 else False

    def open_NC(self, relay_id):
        """Open a connection that's NC (Normally Closed)
        Sets bit as 1, the device is switched OFF after this function is called
        Args:
            relay_id (float): Relay port number 0 - 15
        """
        self.change_bit(relay_id,1)

    def close_NC(self, relay_id):
        """Close a connection that's NC (Normally Closed)
        Sets bit as 0, the device is switched ON after this function is called
        Args:
            relay_id (float): Relay port number 0 - 15
        """
        self.change_bit(relay_id,0)

    def close_NO(self, relay_id):
        """Close a connection that's NO (Normally Open)
        Sets bit as 1, the device is switched ON after this function is called
        Args:
            relay_id (float): Relay port number 0 - 15
        """
        self.change_bit(relay_id,1)

    def open_NO(self, relay_id):
        """Open a connection that's NO (Normally Open)
        Sets bit as 0, the device is switched OFF after this function is called
        Args:
            relay_id (float): Relay port number 0 - 15
        """
        self.change_bit(relay_id,0)


    def twos_complement(self, val, bits):
        """compute the 2's complement of int value val"""
        if (val & (1 << (bits - 1))) != 0:  # if sign bit is set e.g., 8bit: 128-255
            val = val - (1 << bits)        # compute negative value
        return val                         # return positive value as is