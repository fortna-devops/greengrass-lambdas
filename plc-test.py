#!/usr/bin/env python
import sys, logging
import cpppo
from cpppo.server.enip import address, client

logging.basicConfig( **cpppo.log_cfg )
#logging.getLogger().setLevel(logging.INFO)
host            = '192.168.1.1'    # Controller IP address
port            = address[1]       # default is port 44818
depth           = 1                # Allow 1 transaction in-flight
multiple        = 0                # Don't use Multiple Service Packet
fragment        = False            # Don't force Read/Write Tag Fragmented
timeout         = 1.0              # Any PLC I/O fails if it takes > 1s
printing        = False            # Print a summary of I/O
route_path      = [{"link": 0, "port": 1}]

def reboot(id):
    with client.connector( host=host, port=port, timeout=timeout ) as connection:
        operations = client.parse_operations( ["Reboot_Device=(DINT)" + str(id)], route_path=route_path )
        failures,transactions = connection.process(
                            operations=operations, depth=depth, multiple=multiple,
                            fragment=fragment, printing=printing, timeout=timeout )
        return False if failures else True
    return False


def read_plc_string(connection, tag):
        operations = client.parse_operations( [tag + ".LEN"], route_path=route_path )
        failures,transactions = connection.process(
                            operations=operations, depth=depth, multiple=multiple,
                            fragment=fragment, printing=printing, timeout=timeout )
        if failures:
            print('LEN fail=' + str(failures))
            return None
        len = transactions[0][0] # always one transaction and len is a first byte

        operations = client.parse_operations( [tag + ".DATA[0-" + str(len) + "]"], route_path=route_path )
        failures,transactions = connection.process(
                            operations=operations, depth=depth, multiple=multiple,
                            fragment=fragment, printing=printing, timeout=timeout )
        if failures:
            print('DATA fail=' + str(failures))
            return None
        return ''.join(chr(i) for i in transactions[0])


if __name__ == "__main__":
    with client.connector( host=host, port=port, timeout=timeout ) as connection:
        #print(read_plc_string(connection, "strPLC"))
        tags2read = ["T2_F2_1","XR_F1_2PT","T2_F1_4PT","T2_F1_1","PF_F1_5PT","LF_F1_5PT","LF_F1_4","LF_F1_3PT","PF_F1_4","PF_F1_3PT","LF_F1_2","PF_F1_2","LF_F1_1","PF_F2_3PT","PF_F2_4","T1_F1_2PT","PF_F1_6","T1_F1_5","T1_F1_4PT","HC_F1_3PT","HC_F1_2PT","PF_F1_7PT","LF_F1_7PT","T2_F1_6PT","T2_F2_2PT","HC_F1_3PT","XR_F1_2PT","T2_F2_2PT","T1_F1_5","LF_F1_2","PF_F1_2"]
        for tag in tags2read:
            print(tag)
            data_str = read_plc_string(connection,"{t}_UPLOAD".format(t=tag))
            print(data_str)
            try:
                conveyor = data_str.split(',')[0]
                if not conveyor == tag:
                    print("Expected: {c} and {t} to match".format(c=conveyor,t=tag))
            except:
                continue

        #print(read_plc_string(connection, "T2_F2_1_UPLOAD"))
        #print(read_plc_string(connection, "HC_F1_3PT_UPLOAD"))
        #print(read_plc_string(connection, "PF_F1_6_UPLOAD"))
        #print(read_plc_string(connection, "strPLC"))
        #print(read_plc_string(connection, "strPLC"))
        #print(read_plc_string(connection, "strPLC"))
        #print(read_plc_string(connection, "strPLC"))
        #print(reboot(99))

