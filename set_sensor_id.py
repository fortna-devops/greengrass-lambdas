#!/usr/bin/env python
import minimalmodbus
import time
import banner_qm42vt2
import sys


minimalmodbus.BAUDRATE = 19200
#19200
#minimalmodbus.TIMEOUT = 0.1
# port name, slave address (in decimal)

sensors = []
start=1
count=52

port = '/dev/ttyS6'

old_slave_id = 1
new_slave_id = 1

if len(sys.argv)<2:
    print('Insufficient parameters')
    exit()

try:
    new_slave_id = int(sys.argv[1])
    print('New ID= '+str(sys.argv[1]))
except Exception as e:
    print('Invalid parameter= '+str(sys.argv[1]))
    exit()
if(len(sys.argv))==3:
    try:
        old_slave_id = int(sys.argv[2])
        print('Old ID='+str(sys.argv[2]))
    except Exception as e:
        print('Invalid parameter= '+str(sys.argv[2]))
        exit()

try:
    sensor = banner_qm42vt2.BannerQM42VT2(port, old_slave_id)
    sensor.set_slave_id(new_slave_id)
except Exception as e:
    print(str(e))
time.sleep(3)

try:
    sensor = banner_qm42vt2.BannerQM42VT2(port, new_slave_id)
    values = sensor.get_sensor_values()
    print('Reading ID'+str(new_slave_id))
    print(str(values))
    exit()
except Exception as e:
    print(str(e))
    exit()

for i in range(start,start+count):
    sensor = banner_qm42vt2.BannerQM42VT2(port, i)
#    sensor.debug=True
#    sensor.precalculate_read_size=False
#    sensor.serial.stopbits=2
    sensors.append(sensor)
print(sensor)
#for i in range(0,len(sensors)):
#    print(sensors[i])


while True:
    # Register number, number of decimals, function code
    start_time=time.time()
    for i in range(0,len(sensors)):
        try:
            values = sensors[i].get_sensor_values()
            
            print("Slave ID " + str(sensors[i].address)+" "+str(values))
        except Exception as e:
            print(str(e))
            print("unable to read slave id " + str(sensors[i].address))
    print("--------------------Time = "+str(round(time.time()-start_time,2))+" seconds----------------------")
    print("")
    time.sleep(5)
