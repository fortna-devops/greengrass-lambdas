#!/usr/bin/env python
import minimalmodbus
import time
import sealevel_seaio450m
import sys


minimalmodbus.BAUDRATE = 9600
#19200
#minimalmodbus.TIMEOUT = 0.1
# port name, slave address (in decimal)

port = '/dev/ttyS5'
relay_id = 13
while True:
    try:
        relay = sealevel_seaio450m.SeaLevelSeaIO450M(port, 247)
        print(str(relay_id) +' is '+ if relay.is_open_NC(relay_id) 'Open' else 'Closed')
        relay.open_NC(relay_id)
        print(str(relay_id) +' is '+ if relay.is_open_NC(relay_id) 'Open' else 'Closed')
        time.sleep(2)
        relay.close_NC(relay_id)
        print(str(relay_id) +' is '+ if relay.is_open_NC(relay_id) 'Open' else 'Closed')
    except Exception as e:
        print(str(e))
        
    time.sleep(2)