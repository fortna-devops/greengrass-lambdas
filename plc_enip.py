#!/usr/bin/env python

"""

.. moduleauthor:: Dipen Pradhan <dipen_pradhan@mckinsey.com>, Nikolay Khabarov<Nikolay.Khabarov@dataart.com>

EtherNet/IP based driver for the Rockwell ControlLogix 5581 PLC installed 
for data aggregation at FedEx Ground ELOU400 - East Louisville.

"""

import sys, logging, time
import cpppo
from cpppo.server.enip import address, client

__author__ = "Dipen Pradhan, Nikolay Khabarov"
__email__ = "dipen_pradhan@mckinsey.com, Nikolay.Khabarov@dataart.com"
__license__ = "Proprietary Software"


class PLC_ENIP():
    """

    """

    def __init__(self, host, port=address[1], route_path=[]):
        
        logging.basicConfig( **cpppo.log_cfg )
        self.host=host       # Controller IP address
        self.port=port                   # default is port 44818
        self.route_path=route_path
        self.depth           = 1                # Allow 1 transaction in-flight
        self.multiple        = 0                # Don't use Multiple Service Packet
        self.fragment        = False            # Don't force Read/Write Tag Fragmented
        self.timeout         = 1.0              # Any PLC I/O fails if it takes > 1s
        self.printing        = False            # Print a summary of I/O



    def reboot(self, id):
        with client.connector( host=self.host, port=self.port, timeout=self.timeout ) as connection:
            operations = client.parse_operations( ["Reboot_Device=(DINT)" + str(id)], route_path=self.route_path )
            failures,transactions = connection.process(
                                operations=operations, depth=self.depth, multiple=self.multiple,
                                fragment=self.fragment, printing=self.printing, timeout=self.timeout )
            return False if failures else True
        return False

    def read_plc_string(self, connection, tag):
        operations = client.parse_operations( [tag + ".LEN"], route_path=self.route_path )
        failures,transactions = connection.process(
                            operations=operations, depth=self.depth, multiple=self.multiple,
                            fragment=self.fragment, printing=self.printing, timeout=self.timeout )
        if failures:
            return None
        len = transactions[0][0] # always one transaction and len is a first byte

        operations = client.parse_operations( [tag + ".DATA[0-" + str(len) + "]"], route_path=self.route_path )
        failures,transactions = connection.process(
                            operations=operations, depth=self.depth, multiple=self.multiple,
                            fragment=self.fragment, printing=self.printing, timeout=self.timeout )
        if failures:
            return None
        return ''.join(chr(i) for i in transactions[0])
        
    def get_connection(self):
        return client.connector( self.host, self.port, self.timeout)
