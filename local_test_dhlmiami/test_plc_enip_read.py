#!/usr/bin/env python
import sys, logging
import cpppo
from cpppo.server.enip import address, client
import csv

logging.basicConfig( **cpppo.log_cfg )
#logging.getLogger().setLevel(logging.INFO)
host            = '192.168.1.1'    # Controller IP address
port            = address[1]       # default is port 44818
depth           = 1                # Allow 1 transaction in-flight
multiple        = 0                # Don't use Multiple Service Packet
fragment        = False            # Don't force Read/Write Tag Fragmented
timeout         = 1.0              # Any PLC I/O fails if it takes > 1s
printing        = False            # Print a summary of I/O
route_path      = [{"link": 0, "port": 2}]


def reboot(id):
    with client.connector( host=host, port=port, timeout=timeout ) as connection:
        operations = client.parse_operations( ["Reboot_Device=(DINT)" + str(id)], route_path=route_path )
        failures,transactions = connection.process(
                            operations=operations, depth=depth, multiple=multiple,
                            fragment=fragment, printing=printing, timeout=timeout )
        return False if failures else True
    return False


def read_plc_string(connection, tag):
    rp_links = range(0,256)
    rp_ports = range(0,4)
    for rp_link in rp_links:
        for rp_port in rp_ports:
            try_rp = [{"link": rp_link, "port": rp_port}]
            operations = client.parse_operations( [tag + ".LEN"], route_path=try_rp )
            failures,transactions = connection.process(
                                operations=operations, depth=depth, multiple=multiple,
                                fragment=fragment, printing=printing, timeout=timeout )
            if failures:
                print("rp: {}".format(try_rp))
                continue
                return None
            else:
                print("{t}.LEN success parse operation with route path: {rp}".format(t=tag,rp=try_rp))
            len = transactions[0][0] # always one transaction and len is a first byte

            operations = client.parse_operations( [tag + ".DATA[0-" + str(len) + "]"], route_path=try_rp )
            failures,transactions = connection.process(
                                operations=operations, depth=depth, multiple=multiple,
                                fragment=fragment, printing=printing, timeout=timeout )
            if failures:
                continue
                return None
            else:
                print("{t}.DATA success parse operation with route path: {rp}".format(t=tag,rp=try_rp))
            return ''.join(chr(i) for i in transactions[0])


if __name__ == "__main__":
    with client.connector( host=host, port=port, timeout=timeout ) as connection:
        # print(read_plc_string(connection, "strPLC"))

        data = read_plc_string(connection, "T2-F2-1_UPLOAD")
        # data1 = read_plc_string(connection, "XR-F1-2PT_UPLOAD")
        csv_reader = csv.reader([data])
        real_data = list(csv_reader)[0]
        print(real_data)
        # print(read_plc_string(connection, "strPLC"))
        # print(read_plc_string(connection, "strPLC"))
        # print(read_plc_string(connection, "strPLC"))
        # print(read_plc_string(connection, "strPLC"))
        # print(read_plc_string(connection, "strPLC"))
        print(reboot(99))
