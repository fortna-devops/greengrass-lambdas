#!/usr/bin/env python

"""

.. moduleauthor:: Dipen Pradhan <dipen_pradhan@mckinsey.com>

Driver for the Banner Banner QM42VT2 vibration and temperature sensor, for communication via the Modbus RTU protocol.

"""

import minimalmodbus

__author__ = "Dipen Pradhan"
__email__ = "dipen_pradhan@mckinsey.com"
__license__ = "Proprietary Software"


class BannerQM42VT2(minimalmodbus.Instrument):
    """Instrument class for Banner QM42VT2 vibration and temperature sensor.

    Communicates via Modbus RTU protocol (via RS485), using the *MinimalModbus* Python module.

    Banner uses 1-based register addressing, while the Minimalmodbus module uses 0-based register addressing.
    All register addresses in this program will be 1 less than the Banner documentation.
    Example - Temp F is register 5202 in this program, but 5203 in Banner docs

    Args:
        * portname (str): port name
        * slaveaddress (int): slave address in the range 1 to 247

    Implemented with these function codes (in decimal):

    ==================  ====================
    Description         Modbus function code
    ==================  ====================
    Read registers      3
    Write registers     16
    ==================  ====================

    """

    def __init__(self, portname, slaveaddress):
        minimalmodbus.Instrument.__init__(self, portname, slaveaddress)

    # Register Names
    registers_sensor_values = {}

    # Z-Axis RMS Velocity (in/sec)
    Z_RMS_IPS = {'key': 'Z_RMS_IPS', 'register': '5201', 'scale': 10000,
                 'name': 'Z-Axis RMS Velocity', 'unit': 'in/sec'}
    registers_sensor_values[Z_RMS_IPS['register']] = Z_RMS_IPS

    # Z-Axis RMS Velocity (mm/sec)
    Z_RMS_MMPS = {'key': 'Z_RMS_MMPS', 'register': '5202', 'scale': 1000,
                  'name': 'Z-Axis RMS Velocity', 'unit': 'mm/sec'}
    registers_sensor_values[Z_RMS_MMPS['register']] = Z_RMS_MMPS

    # Temperature (F)
    TEMP_F = {'key': 'TEMP_F', 'register': '5203', 'scale': 100,
              'name': 'Temperature', 'unit': 'F', 'signed': True}
    registers_sensor_values[TEMP_F['register']] = TEMP_F

    # Temperature (C)
    TEMP_C = {'key': 'TEMP_C', 'register': '5204', 'scale': 100,
              'name': 'Temperature', 'unit': 'C', 'signed': True}
    registers_sensor_values[TEMP_C['register']] = TEMP_C

    # X-Axis RMS Velocity (in/sec)
    X_RMS_V_IPS = {'key': 'X_RMS_V_IPS', 'register': '5205', 'scale': 10000,
                   'name': 'X-Axis RMS Velocity', 'unit': 'in/sec'}
    registers_sensor_values[X_RMS_V_IPS['register']] = X_RMS_V_IPS

    # X-Axis RMS Velocity (mm/sec)
    X_RMS_V_MMPS = {'key': 'X_RMS_V_MMPS', 'register': '5206', 'scale': 1000,
                    'name': 'X-Axis RMS Velocity', 'unit': 'mm/sec'}
    registers_sensor_values[X_RMS_V_MMPS['register']] = X_RMS_V_MMPS

    # Z-Axis Peak Acceleration (G)
    Z_PEAK_G = {'key': 'Z_PEAK_G', 'register': '5207', 'scale': 1000,
                'name': 'Z-Axis Peak Acceleration', 'unit': 'G'}
    registers_sensor_values[Z_PEAK_G['register']] = Z_PEAK_G

    # X-Axis Peak Acceleration (G)
    X_PEAK_G = {'key': 'X_PEAK_G', 'register': '5208', 'scale': 1000,
                'name': 'X-Axis Peak Acceleration', 'unit': 'G'}
    registers_sensor_values[X_PEAK_G['register']] = X_PEAK_G

    # Z-Axis Peak Velocity Component Frequency (Hz)
    Z_PEAK_V_HZ = {'key': 'Z_PEAK_V_HZ', 'register': '5209', 'scale': 10,
                   'name': 'Z-Axis Peak Velocity Component Frequency', 'unit': 'Hz'}
    registers_sensor_values[Z_PEAK_V_HZ['register']] = Z_PEAK_V_HZ

    # X-Axis Peak Velocity Component Frequency (Hz)
    X_PEAK_V_HZ = {'key': 'X_PEAK_V_HZ', 'register': '5210', 'scale': 10,
                   'name': 'X-Axis Peak Velocity Component Frequency', 'unit': 'Hz'}
    registers_sensor_values[X_PEAK_V_HZ['register']] = X_PEAK_V_HZ

    # Z-Axis RMS Acceleration (G)
    Z_RMS_G = {'key': 'Z_RMS_G', 'register': '5211', 'scale': 1000,
               'name': 'Z-Axis RMS Acceleration', 'unit': 'G'}
    registers_sensor_values[Z_RMS_G['register']] = Z_RMS_G

    # X-Axis RMS Acceleration (G)
    X_RMS_G = {'key': 'X_RMS_G', 'register': '5212', 'scale': 1000,
               'name': 'X-Axis RMS Acceleration', 'unit': 'G'}
    registers_sensor_values[X_RMS_G['register']] = X_RMS_G

    # Z-Axis Kurtosis
    Z_KURT = {'key': 'Z_KURT', 'register': '5213', 'scale': 1000,
              'name': 'Z-Axis Kurtosis', 'unit': ''}
    registers_sensor_values[Z_KURT['register']] = Z_KURT

    # X-Axis Kurtosis
    X_KURT = {'key': 'X_KURT', 'register': '5214', 'scale': 1000,
              'name': 'X-Axis Kurtosis', 'unit': ''}
    registers_sensor_values[X_KURT['register']] = X_KURT

    # Z-Axis Crest Factor
    Z_CREST = {'key': 'Z_CREST', 'register': '5215', 'scale': 1000,
               'name': 'Z-Axis Crest Factor', 'unit': ''}
    registers_sensor_values[Z_CREST['register']] = Z_CREST

    # X-Axis Crest Factor
    X_CREST = {'key': 'X_CREST', 'register': '5216', 'scale': 1000,
               'name': 'X-Axis Crest Factor', 'unit': ''}
    registers_sensor_values[X_CREST['register']] = X_CREST

    # Z-Axis Peak Velocity (in/sec)
    Z_PEAK_V_IPS = {'key': 'Z_PEAK_V_IPS', 'register': '5217', 'scale': 10000,
                    'name': 'Z-Axis Peak Velocity', 'unit': 'in/sec'}
    registers_sensor_values[Z_PEAK_V_IPS['register']] = Z_PEAK_V_IPS

    # Z-Axis Peak Velocity (mm/sec)
    Z_PEAK_V_MMPS = {'key': 'Z_PEAK_V_MMPS', 'register': '5218', 'scale': 1000,
                     'name': 'Z-Axis Peak Velocity', 'unit': 'mm/sec'}
    registers_sensor_values[Z_PEAK_V_MMPS['register']] = Z_PEAK_V_MMPS

    # X-Axis Peak Velocity (in/sec)
    X_PEAK_V_IPS = {'key': 'X_PEAK_V_IPS', 'register': '5219', 'scale': 10000,
                    'name': 'X-Axis Peak Velocity', 'unit': 'in/sec'}
    registers_sensor_values[X_PEAK_V_IPS['register']] = X_PEAK_V_IPS

    # X-Axis Peak Velocity (mm/sec)
    X_PEAK_V_MMPS = {'key': 'X_PEAK_V_MMPS', 'register': '5220', 'scale': 1000,
                     'name': 'X-Axis Peak Velocity', 'unit': 'mm/sec'}
    registers_sensor_values[X_PEAK_V_MMPS['register']] = X_PEAK_V_MMPS

    # Z-Axis High-Frequency RMS Acceleration (G)
    Z_HF_RMS_G = {'key': 'Z_HF_RMS_G', 'register': '5221', 'scale': 1000,
                  'name': 'Z-Axis High-Frequency RMS Acceleration', 'unit': 'G'}
    registers_sensor_values[Z_HF_RMS_G['register']] = Z_HF_RMS_G

    # X-Axis High-Frequency RMS Acceleration (G)
    X_HF_RMS_G = {'key': 'X_HF_RMS_G', 'register': '5222', 'scale': 1000,
                  'name': 'X-Axis High-Frequency RMS Acceleration', 'unit': 'G'}
    registers_sensor_values[X_HF_RMS_G['register']] = X_HF_RMS_G

    def get_sensor_values_raw(self):
        """Return an array of the raw sensor values."""
        # Start reading at 5200 because minimalmodbus uses 0-based addressing and Banner uses 1-based
        return self.read_registers(5200, 22, 3)

    def parse_sensor_values(self, values):
        parsed_values = {}

        for i in range(0, len(values)):
            register = 5201+i
            value = float(values[i]) / self.registers_sensor_values[str(register)]['scale']

            parsed_values[self.registers_sensor_values[str(register)]['key']] = value
        return parsed_values

    def get_sensor_values(self):
        raw_values = self.get_sensor_values_raw()
        values = self.parse_sensor_values(raw_values)
        return values

    def set_slave_id(self, slave_id):
        """Set the sensor's Modbus Slave ID.

        Args:
            value (float): Slave ID
        """
        return self.write_register(6102, slave_id)

    def twos_complement(self, val, bits):
        """compute the 2's complement of int value val"""
        if (val & (1 << (bits - 1))) != 0:  # if sign bit is set e.g., 8bit: 128-255
            val = val - (1 << bits)        # compute negative value
        return val                         # return positive value as is