#!/usr/bin/python2

import os
import arrow
import json

try:
    modem = os.popen("mmcli -L | grep -o -P 'Modem/.{0,2}'").read().split("/")[1]
    signal = os.popen("mmcli -m "+modem.strip()+" | grep -o -P 'signal quality: .{0,4}'").read().split("'")[1]
    print(signal)

    with open("/var/log/cell_signal.log", "a") as cell_signal_log:
        cell_signal_log.write(arrow.now().isoformat()+','+signal+'\n')
        cell_signal_log.close()
    with open("/opt/cellular_signal/signal_quality.txt","w") as signal_quality:
        payload = {}
        payload['read_time']=arrow.now().isoformat()
        payload['signal']=signal
        signal_quality.write(json.dumps(payload))
        signal_quality.close()
except Exception as e:
    print(str(e))