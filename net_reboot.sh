#!/bin/sh

ping -c5 8.8.8.8

if [ $? -eq 0 ]; then
    echo "ok"
else
    /sbin/shutdown -r now
fi